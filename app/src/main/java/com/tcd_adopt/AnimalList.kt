package com.tcd_adopt

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.tcd_adopt.data.DataProviderAnimal
import com.tcd_adopt.data.model.Animal

@Composable
fun AnimalListContent(animal: Animal, navigateToProfile: (Animal) -> Unit)
{
    Card (
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth(),
        elevation = 2.dp,
        backgroundColor = Color.Gray,
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
    ){
        Row(Modifier.clickable { navigateToProfile(animal) })
        {
            AnimalImage(animal)
            Column() {
                Text(text = animal.nomeAnimal, style = typography.h6)
                Text(text = animal.idade)
                Text(text = animal.sexo)
            }
        }
    }
}

@Composable
fun AnimalImage(animal:Animal) {
    Image(
        painter = painterResource(id = animal.id),
        contentDescription = null,
    contentScale = ContentScale.Crop,
        modifier = Modifier
            .padding(8.dp)
            .size(84.dp)
            .clip(RoundedCornerShape(corner = CornerSize(16.dp)))
    )
}
@Preview
@Composable
fun PreviewAnimalItem()
{
    val animal = DataProviderAnimal.animal
    AnimalListContent(animal = animal, navigateToProfile = {})
}
