package com.tcd_adopt

import android.os.Bundle
import android.security.identity.AccessControlProfile
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.tcd_adopt.data.model.Animal
import com.tcd_adopt.ui.theme.Tcd_adoptTheme

class MainActivityCompose : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Tcd_adoptTheme {
                MyApp{

                }
            }
        }
    }
}

@Composable
fun MyApp(navigateToProfile: (Animal) -> Unit) {
    Scaffold(
        content = {
          //  AnimalListContent(navigateToProfile = navigateToProfile)
        }
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Tcd_adoptTheme {
        MyApp{}
    }
}