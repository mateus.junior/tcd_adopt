package com.tcd_adopt

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.BlendMode.Companion.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.tcd_adopt.data.DataProviderAnimal
import com.tcd_adopt.data.DataProviderAnimal.animal
import com.tcd_adopt.data.DataProviderTutor.tutor
import com.tcd_adopt.data.model.Animal

@Composable
fun ScreenProfileAnimal(animal: Animal, onNavIconPressed: () -> Unit = {})
{
    val scrollState = rememberScrollState()

    Column(modifier = Modifier.fillMaxSize()) {
        BoxWithConstraints(modifier = Modifier.weight(1f)) {
            Surface{
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(scrollState),
                ) {
                   ProfileAnimalHeader(
                       scrollState,
                       animal,
                       this@BoxWithConstraints.maxHeight
                   )
                    ProfileContent(animal, this@BoxWithConstraints.maxHeight)
                }
            }
        }
    }
}

@Composable
private fun ProfileContent(animal: Animal, containerHeight: Dp) {
    Column {
        Spacer(modifier = Modifier.height(8.dp))

        Name(animal)

        ProfileProperty(stringResource(R.string.sexo), animal.sexo)

        ProfileProperty(stringResource(R.string.idade), animal.idade.toString())

        ProfileProperty(stringResource(R.string.descricao), animal.descricao)

        ProfileProperty(stringResource(R.string.nome), tutor.nome)
        Spacer(Modifier.height((containerHeight - 320.dp).coerceAtLeast(0.dp)))
    }
}


@Composable
fun ProfileAnimalHeader(
    scrollState: ScrollState,
    animal:Animal,
    containerHeight: Dp)
{
    val offset = (scrollState.value/2)
    val offsetDp = with(LocalDensity.current){offset.toDp()}

    Image(
        modifier = Modifier
            .heightIn(max = containerHeight/2),
            painter = painterResource(id = animal.drawableImg),
            contentScale = ContentScale.Crop,
            contentDescription = null
        )
}

@Composable
fun ProfileProperty(label: String, value: String, isLink: Boolean = false) {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Divider()
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            Text(
                text = label,
                modifier = Modifier.paddingFromBaseline(24.dp),
                style = MaterialTheme.typography.caption,
            )
        }
        val style = if (isLink) {
            MaterialTheme.typography.body1.copy(color = MaterialTheme.colors.primary)
        } else {
            MaterialTheme.typography.body1
        }
        Text(
            text = value,
            modifier = Modifier.paddingFromBaseline(24.dp),
            style = style
        )
    }
}

@Composable
private fun Name(animal: Animal, modifier: Modifier = Modifier) {
    Text(
        text = animal.nomeAnimal,
        modifier = modifier,
        style = MaterialTheme.typography.h5,
        fontWeight = FontWeight.Bold
    )
}

@Composable
private fun Name(
    animal: Animal
) {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Name(
            animal = animal,
            modifier = Modifier.paddingFromBaseline(32.dp)
        )
    }
}


@Composable
fun AdoptFab(extended: Boolean, modifier: Modifier = Modifier) {
    FloatingActionButton(
        onClick = { /* TODO */ },
        modifier = modifier
            .padding(16.dp)
            .padding()
            .height(48.dp)
            .widthIn(min = 48.dp),
        backgroundColor = androidx.compose.ui.graphics.Color.Magenta,
        contentColor = androidx.compose.ui.graphics.Color.White
    ) {

    }
}

@Preview
@Composable
fun ProfilePreview() {
    val animal = DataProviderAnimal.animal
    ScreenProfileAnimal(animal = animal)
}
