package com.tcd_adopt.data.model

data class Tutor(
    val id:Int,
    val nome: String,
    val idade: Int,
    val cidade: String,
    val estado: String,
    val bairro: String,
    val rua: String,
    val numeroRua: Int,
    val celular: String
)
