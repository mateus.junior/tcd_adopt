package com.tcd_adopt.data.model

import androidx.annotation.DrawableRes

data class Animal(
    val id: Int,
    val nomeAnimal: String,
    val porte: String,
    val idade: String,
    val idTutor: Int,
    val sexo: String,
    val descricao: String,
    val vacinaCheck: Boolean,
    @DrawableRes val drawableImg: Int
)
