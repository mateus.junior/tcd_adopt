package com.tcd_adopt.data

import com.tcd_adopt.data.model.Tutor
object DataProviderTutor {
    val tutor = Tutor(
        id = 0,
        nome = "Vinicius",
        idade = 15,
        cidade = "Uberlândia",
        estado = "MG",
        bairro = "Santa Mônica",
        rua = "Rua Paradeiro",
        numeroRua = 512,
        celular = "(34) 98322-7667"
    )
    val tutorList = listOf(
        tutor,
        Tutor(
            id = 1,
            nome = "Mateus",
            idade = 21,
            cidade = "Uberaba",
            estado = "MG",
            bairro = "Conj Uberaba",
            rua = "Rua Oscarina de Castro",
            numeroRua = 901,
            celular = "(34) 98420-5687"
        ),
        Tutor(
            id = 2,
            nome = "Lucilander",
            idade = 23,
            cidade = "Uberaba",
            estado = "MG",
            bairro = "Bairro de Lourdes",
            rua = "Rua Lima Rodrigues",
            numeroRua = 412,
            celular = "(34) 94820-5867"
        ),
        Tutor(
            id = 3,
            nome = "Lenner",
            idade = 23,
            cidade = "Uberaba",
            estado = "MG",
            bairro = "Valim de Melo",
            rua = "Rua Oscar Marcelo",
            numeroRua = 1001,
            celular = "(34) 93220-5657"
        ),
        Tutor(
            id = 4,
            nome = "Felipe",
            idade = 25,
            cidade = "Uberaba",
            estado = "MG",
            bairro = "Fabricio",
            rua = "Rua Horizonte",
            numeroRua = 513,
            celular = "(34) 98395-5107"
        ),
    )
}