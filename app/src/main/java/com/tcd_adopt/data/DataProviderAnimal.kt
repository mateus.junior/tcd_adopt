package com.tcd_adopt.data

import com.tcd_adopt.R
import com.tcd_adopt.data.model.Animal
object DataProviderAnimal {
    val animal =
        Animal(
            id = 0,
            nomeAnimal = "Ralph",
            porte = "Médio",
            idade = "5m",
            idTutor = 2,
            sexo = "Masculino",
            descricao = "Cachorro Dócil e bem cuidado.",
            vacinaCheck = true,
            drawableImg = R.drawable.alex
        )
    val animalList = listOf(
        animal,
        Animal(
            id = 1,
            nomeAnimal = "Bolinha",
            porte = "Médio",
            idade = "3m",
            idTutor = 0,
            sexo = "Masculino",
            descricao = "Cachorro Dócil e bem cuidado.",
            vacinaCheck = true,
            drawableImg = R.drawable.alex
        ),
        Animal(
            id = 2,
            nomeAnimal = "Feelipe",
            porte = "Grande",
            idade = "4a",
            idTutor = 0,
            sexo = "Masculino",
            descricao = "Cachorro muito carinhoso e manso.",
            vacinaCheck = true,
            drawableImg = R.drawable.alex
        ),
        Animal(
            id = 3,
            nomeAnimal = "Arthur",
            porte = "Médio",
            idade = "6m",
            idTutor = 1,
            sexo = "Masculino",
            descricao = "Lindo cachorro precisando de atenção e abrigo.",
            vacinaCheck = true,
            drawableImg = R.drawable.alex
        ),

    )
}